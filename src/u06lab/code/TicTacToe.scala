package u06lab.code

object TicTacToe extends App {

  sealed trait Player {
    def other: Player = this match {
      case X => O;
      case _ => X
    }

    override def toString: String = this match {
      case X => "X";
      case _ => "O"
    }
  }

  case object X extends Player

  case object O extends Player

  case class Mark(x: Int, y: Int, player: Player)

  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Int, y: Int): Option[Player] = board.collectFirst { case mark if mark.x == x && mark.y == y => mark.player }

  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    var result: Seq[Board] = Seq()
    for (x <- 0 to 2; y <- 0 to 2 if find(board, x, y).isEmpty)
      result = result :+ (Mark(x, y, player) :: board)
    result
  }

  def computeAnyGame(player: Player, moves: Int): LazyList[Game] = moves match {
    case 0 => LazyList(List(List()))
    case _ => for {
      game <- computeAnyGame(player.other, moves - 1)
      board <- if (!hasSomeoneWon(game.head)) placeAnyMark(game.head, player) else List(Nil)
    } yield if (!hasSomeoneWon(game.head)) board :: game else game
    /*computeAnyGame(player.other, moves - 1)
    .flatMap(g => {
      (if (!hasSomeoneWon(g.head)) placeAnyMark(g.head, player) else List(Nil))
        .map(b => if (!hasSomeoneWon(g.head)) b :: g else g)
    })*/
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ".")
      if (x == 2) {
        print(" "); if (board == game.head) println()
      }
    }

  private def hasSomeoneWon(board: Board): Boolean = {
    var hasWon = false
    for (row <- 0 to 2 if !hasWon) {
      val p1 = find(board, row, 0)
      val p2 = find(board, row, 1)
      val p3 = find(board, row, 2)
      hasWon = checkWin(p1, p2, p3)
    }
    for (column <- 0 to 2 if !hasWon) {
      val p1 = find(board, 0, column)
      val p2 = find(board, 1, column)
      val p3 = find(board, 2, column)
      hasWon = checkWin(p1, p2, p3)
    }
    if (!hasWon){
      val p1 = find(board, 0, 0)
      val p2 = find(board, 1, 1)
      val p3 = find(board, 2, 2)
      hasWon = checkWin(p1, p2, p3)
    }
    if (!hasWon){
      val p1 = find(board, 2, 0)
      val p2 = find(board, 1, 1)
      val p3 = find(board, 0, 2)
      hasWon = checkWin(p1, p2, p3)
    }
    hasWon
  }

  private def checkWin(p1: Option[Player], p2: Option[Player], p3: Option[Player]): Boolean = p1.isDefined && p2.isDefined && p3.isDefined && p1.get == p2.get && p2.get == p3.get

  // Exercise 1: implement find such that..
  println(find(List(Mark(0, 0, X)), 0, 0)) // Some(X)
  println(find(List(Mark(0, 0, X), Mark(0, 1, O), Mark(0, 2, X)), 0, 1)) // Some(O)
  println(find(List(Mark(0, 0, X), Mark(0, 1, O), Mark(0, 2, X)), 1, 1)) // None

  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(), X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0, 0, O)), X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O, 6) foreach { g => printBoards(g); println() }
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. OOO OOO
  //... ... ... ... ... O..
  //... .X. .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
  val testBoard = List(Mark(0, 0, O), Mark(0, 1, O), Mark(0, 2, O))
  printBoards(List(testBoard))
  println(hasSomeoneWon(testBoard))
}